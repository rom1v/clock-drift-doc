# Clock drift

A clock drift is typically on the order of a few seconds per day. On Kyber, we
observed 3.5 seconds per day (which is a significant drift).

Since the drift between two clocks is a difference in slope, one might have the
illusion that the clock slope estimation is what allows to compensate for the
drift (so if we no longer estimate the slope, we no longer compensate for the
drift).

But that's not the case: given the magnitudes, what compensates for the drift is
the constant re-estimation.

If we made a fairly precise slope estimation and did not re-estimate, the
clock would drift enormously. Conversely, if we make a more or less fanciful
(reasonably) estimate of the slope, the long-term drift would still be
compensated for.

By the way, on recent work to improve clock estimation using least squares, the
result is rather good compared to before:

> coeff in range [0.999679; 1.000359], diff: 0.00068

But if we look at how much the small variations in this estimate represent, it
corresponds to an estimation error of almost 1 minute per day (86400×0.00068),
so an "errer" several tens of times the drift it is supposed to compensate for.

This is much better than the "legacy" estimate:

> coeff in range [0.991215; 1.008741], diff: 0.017526

which introduces an estimation error in drift of over 25 minutes per day.

But in practice, it's not a big deal since this is not what compensates for the
drift in the long term. And even these estimation errors (which introduce an
additional huge "rift") are absorbed by the constant re-estimations (the clock
only estimates very close points in the future).

By analogy, it's like wanting to drive towards a point 10 km away in a car,
taking into account a crosswind. One might think that the car's orientation
needs to be extremely precise (an error of 1°, after 10 km, is 175 meters of
error). But in reality, it is enough to move towards the destination and
regularly adjust the orientation throught the journey to compensate (by turning
the steering wheel, even if it's not super precise). And throughout the journey,
it's pointless to estimate the exact angle to apply to compensate for the wind
over the entire route (especially if this estimate introduces an error 10 or 50
times the impact of the wind itself).

However, if one had to launch a missile (or lock the car's steering wheel from
the start), then an ultra-precise measurement of the angle at the start would be
necessary to have a chance of reaching the target. But we are not in that case
for a multimedia player.

---

To intuitively understand why regular re-estimation allows for accounting for
drift in the long term, I quickly sketched a small graph:

![graph](graph.png)

The black line represents the actual clock that is drifting (whose slope is not
known a priori).

The red segments are the successive estimates. _In practice, you must imagine
many red segments, much smaller, because re-estimation occurs all the time._

The slopes of the estimates are not the same as the actual slope of the drifting
clock. Nevertheless, each successive estimate falls on the black line at the
moment it is measured: there is no eventual divergence due to drift.

Estimating the slope for each successive estimates just slightly changes the
slope of the tiny red segments, so it has no impact on drift compensation in the
long term.

Even if drift estimation were perfect, its value is totally negligible to
estimate a point near in the future: a drift of 1 second per day
applied to a point 1 second in the future represents a correction of 11µs.
